package uco_75383.movio2.pv256.fi.muni.cz.skolniprojekt;

import android.app.Application;
import android.os.StrictMode;

public class App extends Application{
    @Override
    public void onCreate() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().detectAll().build();
        StrictMode.setThreadPolicy(policy);
        StrictMode.VmPolicy vmPolicy = new StrictMode.VmPolicy.Builder().detectAll().build();
        StrictMode.setVmPolicy(vmPolicy);
        super.onCreate();
    }
}
